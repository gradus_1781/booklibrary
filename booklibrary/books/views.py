from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from .models import Books, Authors, Genres
from .forms import MapCreateForm, AuthorCreateForm, GenreCreateForm, UserRegistrationForm

# Create your views here.
def main_page(request):
    return render(request, "main_page.html")


class BookListView(ListView):
    model = Books
    paginate_by = 3


class BookDetailView(DetailView):
    model = Books


class BookCreateView(CreateView):
    model = Books
    form_class = MapCreateForm
    success_url = reverse_lazy('books_list')


class BookDeleteView(DeleteView):
    model = Books
    success_url = reverse_lazy('books_list')


class BookUpdateView(UpdateView):
    model = Books
    form_class = MapCreateForm
    template_name_suffix = '_update_form'
    succes_url = reverse_lazy('books_list')


class AuthorListView(ListView):
    model = Authors
    paginate_by = 5
    template_name = 'authors/authors_list.html'


class AuthorDetailView(DetailView):
    model = Authors
    template_name = 'authors/authors_detail.html'

    def get_context_data(self, **kwargs):
        context = super(AuthorDetailView, self).get_context_data(**kwargs)
        context['book'] = Books.objects.filter(author__first_name__icontains=context['authors'].first_name)
        return context


class AuthorCreateView(CreateView):
    model = Authors
    form_class = AuthorCreateForm
    template_name = 'authors/authors_form.html'
    success_url = reverse_lazy('authors_list')


class AuthorUpdateView(UpdateView):
    model = Authors
    form_class = AuthorCreateForm
    template_name = 'authors/authors_update_form.html'
    success_url = reverse_lazy('authors_list')


class AuthorDeleteView(DeleteView):
    model = Authors
    template_name = 'authors/authors_confirm_delete.html'
    success_url = reverse_lazy('books_list')


class GenreListView(ListView):
    model = Genres
    template_name = 'genres/genres_list.html'

    def get_queryset(self): 
        return Genres.objects.order_by('name')

    def get_context_data(self, **kwargs):
        context = super(GenreListView, self).get_context_data(**kwargs)
        genre_filter = self.request.GET.getlist('genre')
        if genre_filter != '' and genre_filter is not None:
            # looping, because model have Many to Many relation
            for genre in genre_filter:
                context['book_list'] = Books.objects.filter(genre__name=genre)
                context['genre_name'] = Genres.objects.filter(name=genre)
        return context


class GenreCreateView(CreateView):
    model = Genres
    form_class = GenreCreateForm
    template_name = 'genres/genres_form.html'
    success_url = reverse_lazy('genres_list')


class GenreUpdateView(UpdateView):
    model = Genres
    form_class = GenreCreateForm
    template_name = 'genres/genres_update_form.html'
    success_url = reverse_lazy('genres_list')


class GenreDeleteView(DeleteView):
    model = Genres
    template_name = 'genres/genres_confirm_delete.html'
    success_url = reverse_lazy('genres_list')


class UserRegistrationView(CreateView):
    form_class = UserRegistrationForm
    template_name = "registration/register.html"
    success_url = reverse_lazy("register_done")


def register_done(request):
    return render(request, "registration/register_done.html")
