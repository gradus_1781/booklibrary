from django.db import models
from django.urls import reverse


class Genres(models.Model):
    name = models.CharField(
        verbose_name="Жанр книги",
        max_length=32,
        blank=False,
        null=False,
        unique=True,
    )

    description = models.TextField(
        verbose_name="Описание жанра",
        default="без описания",
        blank=False,
        null=False,
    )

    def __str__(self):
        return self.name


class Authors(models.Model):
    first_name = models.CharField(
        verbose_name="Имя",
        max_length=32,
        blank=False,
        null=False,
    )
    last_name = models.CharField(
        verbose_name="Фамилия",
        max_length=32,
        blank=False,
        null=False,
    )
    date_of_birth = models.DateField(
        verbose_name="Дата рождения",
        blank=True,
        null=True,
    )
    date_of_death = models.DateField(
        verbose_name="Дата смерти",
        blank=True,
        null=True,
    )
    description = models.TextField(
        verbose_name="Краткое описание",
        blank=False,
        null=False,
    )
    portrait = models.ImageField(
        verbose_name="Портрет",
        upload_to='portraits',
    )

    def __str__(self):
        return f"{self.last_name} {self.first_name}"
    
    
    def get_absolute_url(self):
        """
        Returns URL-address to access a specific author instance 
        """
        return reverse("author-detail", args=[str(self.id)])


class Books(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=50,
        blank=False,
        null=False,
    )
    author = models.ManyToManyField(
        "Authors",
        verbose_name="Автор(ы)",
        blank=False,
    )
    genre = models.ManyToManyField(
        "Genres",
        verbose_name="Жанры",
        blank=False,
    )
    date_created = models.DateField(
        verbose_name="Дата написания книги",
        blank=True,
        null=True
    )
    description = models.TextField(
        verbose_name="Краткое описание",
        blank=False,
        null=False,
    )
    book_file = models.FileField(
        verbose_name="Файл книги",
        upload_to="books_files_pdf",
    )
    book_image = models.ImageField(
        verbose_name="Изображение книги",
        upload_to="books_images",
    )


    def display_author(self):
        return ', '.join([f'{author.first_name} {author.last_name}' for author in self.author.all()])

    display_author.short_description = 'Автор(ы)'

    def display_genre(self):
        return ', '.join([genre.name for genre in self.genre.all()])

    
    def __str__(self):
        return self.name
    

    def get_absolute_url(self):
        """
        Returns URL-address to access a specific book instance 
        """
        return reverse("book-detail", args=[str(self.id)])
    