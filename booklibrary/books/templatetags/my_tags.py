from django import template


register = template.Library()

@register.simple_tag(takes_context=True)
def param_replace(context, **kwargs):
    """
    Return encoded URL parameters that are the same as the current
    request's parameters, only with the specified GET parameters added or changed.
    """

    d = context['request'].GET.copy()
    for k, v in kwargs.items():
        d[k] = v

    for k in [k for k, v in d.items() if not v]:
        del d[k]

    return d.urlencode()


def sizify(value):
    """
    Simple kb/mb/gb size snippet for templates:   
    {{ product.file.size|sizify }}
    """
    if value < 512000:
        value = value / 1024.0
        ext = 'kb'
    elif value < 4194304000:
        value = value / 1048576.0
        ext = 'mb'
    else:
        value = value / 1073741824.0
        ext = 'gb'

    return f"{round(value, 2)} {ext}"


register.filter('sizify', sizify)
