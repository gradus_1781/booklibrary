from django.urls import path
from .views import BookListView, BookDetailView, BookCreateView, BookDeleteView, BookUpdateView


urlpatterns = [
    path('', BookListView.as_view(), name='books_list'),
    path('book/<int:pk>', BookDetailView.as_view(), name='book-detail'),
    path('create/', BookCreateView.as_view(), name='book_create'),
    path('delete/<int:pk>', BookDeleteView.as_view(), name='delete_book'),
    path('update/<int:pk>', BookUpdateView.as_view(), name='update_book'),
]
