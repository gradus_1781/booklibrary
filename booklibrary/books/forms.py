from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import Authors, Books, Genres


class MapCreateForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = (
            "name", "author", "date_created", "genre",
            "description", "book_file", "book_image",
        )

        widgets = {
            "name": forms.TextInput(attrs={"class": "form-control"}),
            "author": forms.CheckboxSelectMultiple(attrs={'class': 'form-check form-check-inline'}),
            "date_created": forms.widgets.DateInput(attrs={
                "class": "form-control",
                "type": "date"
                }),
            "genre": forms.CheckboxSelectMultiple(attrs={'class': 'form-check form-check-inline'}),
            "description": forms.Textarea(attrs={'class': 'form-control'}),
            "book_file": forms.FileInput(attrs={"class": "form-control"}),
            "book_image": forms.FileInput(attrs={"class": "form-control"}),
        }


class AuthorCreateForm(forms.ModelForm):
    class Meta:
        model = Authors
        fields = (
            "first_name", "last_name", "date_of_birth",
            "date_of_death", "description", "portrait",
        )

        widgets = {
            "first_name": forms.TextInput(attrs={"class": "form-control"}),
            "last_name": forms.TextInput(attrs={"class": "form-control"}),
            "date_of_birth": forms.widgets.DateInput(attrs={
                "class": "form-control",
                "type": "date"
                }),
            "date_of_death": forms.widgets.DateInput(attrs={
                "class": "form-control",
                "type": "date"
                }),
            "description": forms.Textarea(attrs={'class': 'form-control'}),
            "portrait": forms.FileInput(attrs={"class": "form-control"}),
        }


class GenreCreateForm(forms.ModelForm):
    class Meta:
        model = Genres
        fields = (
            "name", "description"
        )
        
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-control"}),
            "description": forms.Textarea(attrs={"class": "form-control"}),
        }


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email')
        