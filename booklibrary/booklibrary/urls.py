from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from books.views import (main_page, register_done, AuthorListView, AuthorDetailView,
                        AuthorCreateView, AuthorUpdateView, AuthorDeleteView,
                        GenreListView, GenreCreateView, GenreUpdateView,
                        GenreDeleteView, UserRegistrationView)


urlpatterns = [
    path("", main_page, name="main"),
    path("books/", include('books.urls')),
    path('authors/', AuthorListView.as_view(), name='authors_list'),
    path('authors/<int:pk>', AuthorDetailView.as_view(), name='author-detail'),
    path('authors/create/', AuthorCreateView.as_view(), name='author_create'),
    path('authors/update/<int:pk>', AuthorUpdateView.as_view(), name='update_author'),
    path('authors/delete/<int:pk>', AuthorDeleteView.as_view(), name='delete_author'),
    path('genres/', GenreListView.as_view(), name='genres_list'),
    path('genres/create/', GenreCreateView.as_view(), name='genre_create'),
    path('genres/update/<int:pk>', GenreUpdateView.as_view(), name='update_genre'),
    path('genres/delete/<int:pk>', GenreDeleteView.as_view(), name='delete_genre'),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/register/', UserRegistrationView.as_view(), name='register'),
    path('accounts/register/register_done/', register_done, name='register_done'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
