About project:
Educational site with the ability to enter / register, view / change content (books, authors)


Getting started:
-- Creating virtual environment:
python{version} -m venv {name of venv}

example:
python3.10 -m venv env

-- Enter to virtual environment:
venv\Scripts\activate.bat - for Windows;
source venv/bin/activate - for Linux and MacOS.

-- Clone repository:
git clone https://gitlab.com/gradus_1781/booklibrary.git

-- Enter to project:
cd booklibrary

-- Install dependencies:
pip install -r pip_requirements.txt

-- Enter deeper to project:
cd booklibrary

-- Launch project:
python manage.py runserver

Now u can enter to localhost site and observe it:
http://localhost:8000
